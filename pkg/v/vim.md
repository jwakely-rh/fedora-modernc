* [patch 8.2.5135: running configure gives warnings for main() return type](https://github.com/vim/vim/commit/0f0d3a7fb6473760b6f6679e3c8a81376220c869)
* [patch 9.0.0834: warning for missing return type](https://github.com/vim/vim/commit/f8ea10677d007befbb6f24cd20f35c3bf71c1296)
* https://src.fedoraproject.org/rpms/vim/c/09c5bd5eb03db6538e43797c6a151374b8ae412f?branch=rawhide
