* [C99 compatibility fix for the configure script](https://sourceforge.net/p/surf/bugs/19/)
* [C99 fix for parser/lexer interface](https://sourceforge.net/p/surf/bugs/20/)
* https://src.fedoraproject.org/rpms/surf-geometry/c/4831c9869f22eac3e3cb5bf8c601cdacfe39bb12?branch=rawhide
