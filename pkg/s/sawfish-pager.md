* [sawfish-pager: Re-enable deprecated GTK/GDK features during build](https://bugzilla.redhat.com/show_bug.cgi?id=2153033)
* https://src.fedoraproject.org/rpms/sawfish-pager/c/0bbbffb0f998d6ab1d5365c94e53cdf3f6922984?branch=rawhide
