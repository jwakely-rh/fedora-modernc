* https://bugzilla.redhat.com/show_bug.cgi?id=1999484
* [srmio.h: Include <time.h> unconditionally](https://github.com/rclasen/srmio/pull/3)
* https://src.fedoraproject.org/rpms/srmio/c/059bcea7b172a360ef394788d93717b9284ae28a?branch=rawhide
