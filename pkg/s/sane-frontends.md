* [scanadf: fixes for various compilation warnings](https://gitlab.com/fweimer-rh/frontends/-/commit/7ebdf2cd6232aa22c78fd9b59951a3e9ee43a715)
* [C99 compatibility fixes](https://gitlab.com/sane-project/frontends/-/merge_requests/9)
* https://src.fedoraproject.org/rpms/sane-frontends/c/107104d01ba0f0590e4b36497f344fa63cdf46b7?branch=rawhide
