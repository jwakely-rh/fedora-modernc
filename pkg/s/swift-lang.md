* [Build process.c with _GNU_SOURCE](https://github.com/apple/swift-tools-support-core/commit/4727bc60e18530c8117c8d4e5512d9fabdc71e97)

Not backported into rawhide because the issue is hidden after
[a change in glibc](https://sourceware.org/git/?p=glibc.git;a=commitdiff;h=2ff48a4025515e93d722947a9eabb114f4a65b22).
