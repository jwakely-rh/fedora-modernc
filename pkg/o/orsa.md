* Upstream has switched to CMake, eliminating the problematic configure check.
* https://src.fedoraproject.org/rpms/orsa/c/59c4090a92905175d778d763612e7e42aa08ee1a?branch=rawhide
