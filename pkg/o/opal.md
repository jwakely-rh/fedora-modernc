* [Fixed a raft of issues with XCode 5 and Mountain Lion.](https://sourceforge.net/p/opalvoip/opal/ci/edeb5682d5b046bc48abf1773eb99799c7962aa4/)
* https://src.fedoraproject.org/rpms/opal/c/fc73c11d99d89801dae07e8d0cbb17edec8534fe?branch=rawhide
