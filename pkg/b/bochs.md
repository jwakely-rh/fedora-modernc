* [configure: Improve compatibility with strict(er) C99 compilers](https://github.com/bochs-emu/Bochs/pull/20)
* [Port configure script to C99](https://sourceforge.net/p/bochs/patches/566/) (legacy Sourceforge repository)
* https://src.fedoraproject.org/rpms/bochs/c/245fa1c25779021e9c5830f685bdc4c3e650ddc8?branch=rawhide
