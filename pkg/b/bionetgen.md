* [Bundled CVODE contains CMake script with C99 compatibility issue](https://github.com/RuleWorld/bionetgen/issues/262)
* https://src.fedoraproject.org/rpms/bionetgen/c/6e8b4f8e20ca4d3424c011b8267ff298fa69f328?branch=rawhide
