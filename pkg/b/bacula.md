* [autoconf: Do not call exit in the getaddrinfo check](https://gitlab.bacula.org/bacula-community-edition/bacula-community/-/issues/2666)
* https://src.fedoraproject.org/rpms/bacula/c/4b8761304540894df455fd6d3ca91f8f3e89141d?branch=rawhide
