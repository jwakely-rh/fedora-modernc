* [Add missing header to fix C99 conformance in blockmode.c](https://sourceforge.net/p/beecrypt/patches/13/)
* [Fix C99 conformance issues in autotools.](https://sourceforge.net/p/beecrypt/patches/12/)
* https://src.fedoraproject.org/rpms/beecrypt/c/c063d876b3ac038ba1a624cd9dc0b96bff4ebef0?branch=rawhide
