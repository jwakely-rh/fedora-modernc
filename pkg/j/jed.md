* [Avoid implicit function declaration in selinux.c](https://src.fedoraproject.org/rpms/jed/c/06ddecb7f42170882f9730d8955e03ad1e4082a6#_2) (issue in Fedora's SELinux patch)
* https://src.fedoraproject.org/rpms/jed/c/06ddecb7f42170882f9730d8955e03ad1e4082a6?branch=rawhide
