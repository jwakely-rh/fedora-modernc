* [Improve C99 compatibility of code-generating shell scripts](https://github.com/kmatheussen/jack_capture/pull/52)
* https://src.fedoraproject.org/rpms/jack_capture/c/debe475ce39ca789595f884aa15c059fa91ad267?branch=rawhide
