* [test: Improve C99 compatibility of sofia-sip workaround](https://gitlab.gnome.org/GNOME/calls/-/merge_requests/658)
* https://src.fedoraproject.org/rpms/calls/c/0d6bf1478eb2394a20f4bd4ae94714bdac28e916?branch=rawhide
