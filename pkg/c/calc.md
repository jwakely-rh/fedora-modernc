* [Avoid implicit declaration memset in have_fpos_pos.c probe](https://github.com/lcn2/calc/pull/68)
* https://src.fedoraproject.org/rpms/calc/c/13722b29e57dcca032737dea044a3103ff99aff8?branch=rawhide
