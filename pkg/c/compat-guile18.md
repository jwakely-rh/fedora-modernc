* [configure.ac fix for C99 compatibility](https://debbugs.gnu.org/cgi/bugreport.cgi?bug=60022), [mailing list thread](https://lists.gnu.org/archive/html/bug-guile/2022-12/msg00017.html)
* https://src.fedoraproject.org/rpms/compat-guile18/c/431e4bcd0d1e34a4360f8d757037f901c704459d?branch=rawhide
