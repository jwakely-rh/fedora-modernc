* [Fix building with C99 compilers](https://github.com/CristianHenzel/ClipIt/pull/203)
* https://src.fedoraproject.org/rpms/clipit/c/f0952b6ce182351a2fa53ea9a1f572c8e9af75ef?branch=rawhide
