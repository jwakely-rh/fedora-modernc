* [configure.ac: Improve C99 compatibility of NETSNMP_USE_OPENSSL check](https://github.com/Cacti/spine/pull/290)
* https://src.fedoraproject.org/rpms/cacti-spine/c/df744be2b2724ee67b113fd83c7ef8b0350dfb0c?branch=rawhide
