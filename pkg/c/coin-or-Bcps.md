* [Re-running autotools, moving to auto-generation of README, moving to Github Actions from Travis and Appveyor](https://github.com/coin-or/CHiPPS-BiCePS/commit/da5df6ba4a7e8626e6aaf41898406262a1cdf664)
* [Removing old files that are no longer needed](https://github.com/coin-or/CHiPPS-BiCePS/commit/a28ade7597645136fbfe6b8baa50002bbae947ea)
* https://src.fedoraproject.org/rpms/coin-or-Bcps/c/d7e0a29d4bb4145e7417c17e6d76d4273b49a237?branch=rawhide
