* [config: Fix -Wimplicit-function-declaration](https://git.netfilter.org/conntrack-tools/commit/?id=6ce497caac85f53a54e359ca57ad0f9dc379021f)
* https://src.fedoraproject.org/rpms/conntrack-tools/c/bdc967187bab94c802285d99d398a6e3b776f989?branch=rawhide
