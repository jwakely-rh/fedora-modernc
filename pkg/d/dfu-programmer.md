* [Missing #include <string.h> in AC_FUNC_MEMCMP](https://savannah.gnu.org/support/index.php?110530)
* [Misuse of autoconf features in configure.ac causes multiple problems](https://github.com/dfu-programmer/dfu-programmer/issues/61)
* [Remove libusb0 support](https://github.com/dfu-programmer/dfu-programmer/pull/66)
* [src/atmel.c: Fix compilation with strict(er) C99 compilers](https://github.com/dfu-programmer/dfu-programmer/pull/76)
* https://src.fedoraproject.org/rpms/dfu-programmer/c/aeec67240f42b2dfc9535ee7036c2f46627e7bca?branch=rawhide
