* [C99 compatibility fix for drpm_write.c](https://github.com/rpm-software-management/drpm/pull/20)
* https://src.fedoraproject.org/rpms/drpm/c/fba8343f02e91e3dcceeacbe22900662b1780ffa?branch=rawhide
