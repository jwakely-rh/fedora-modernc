* https://src.fedoraproject.org/rpms/drat-trim/c/a9183b0ef79daad828d139d7d9a998524f925d55?branch=rawhide

Setting `CFLAGS` overrides `-std=c99` in the upstream makefile.
Without `std=c99`, `<stdio.h>` declares `getc_unlocked`, fixing the
issue.
