* [configure.ac: fix configure tests broken with Clang 15 (implicit function declarations)](https://github.com/the-tcpdump-group/tcpdump/commit/6de7b5fa705106f9299e8892f9c0cb3a1ed2a5dd)
* https://src.fedoraproject.org/rpms/tcpdump/c/be13d43b6f881f43f10b3718b07871971c724075?branch=rawhide
