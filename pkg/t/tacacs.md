* [Add more function prototypes to avoid implicit function declarations](https://github.com/facebook/tac_plus/pull/36)
* https://src.fedoraproject.org/rpms/tacacs/c/f4f5056d47e20e28d664b0ec40c47dfa78efbbb3?branch=rawhide
