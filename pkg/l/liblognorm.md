* https://bugzilla.redhat.com/show_bug.cgi?id=2141801
* https://src.fedoraproject.org/rpms/liblognorm/c/cdaf4de866002343f4011bb05c71f764258948a7?branch=rawhide

The `configure` glitch is harmless, but this avoids triggering a
problem report during tests.
