*  Add SONAME versions generation for debian distributions](https://github.com/IntelRealSense/librealsense/commit/8b29ff22d92f6639809d59060712ebaa0792a9a2) (specifically, *Remove invalid std=c11 compiler flag*)
* https://src.fedoraproject.org/rpms/librealsense1/c/9ee214e275743b47301dc2d1c51dddc5f5c66d6d?branch=rawhide

This is likely a real bug that causes crashes.
