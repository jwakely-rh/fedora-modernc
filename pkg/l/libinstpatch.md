* [libinstpatch: Missing declaration of ipatch_init in generate file docs/reference/libinstpatch-scan.c ](https://bugzilla.redhat.com/show_bug.cgi?id=2161997)
* [Fix warning from libinstpatch-scan.c (gtkdoc)](https://github.com/swami/libinstpatch/pull/71)
* https://src.fedoraproject.org/rpms/libinstpatch/c/0553f9ab54f6b0d4f811f388ea32c80f6f74d2b8?branch=rawhide
