* [Avoid relying on C89 features in a few places](https://gitlab.com/samba-team/samba/-/merge_requests/2807) (submitted against Samba)
* https://src.fedoraproject.org/rpms/libtdb/tree/rawhide
