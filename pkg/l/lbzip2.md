* [Define the GNULIB_XALLOC_DIE macro](https://github.com/kjn/lbzip2/pull/33)
* [Re: xmalloc calling undeclared xalloc_die function](https://lists.gnu.org/archive/html/bug-gnulib/2022-12/msg00038.html)
* https://src.fedoraproject.org/rpms/lbzip2/c/84570299d30750c5664e6f3169d20d7b1f34589a?branch=rawhide
