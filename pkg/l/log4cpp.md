* [C99 compatibility fix for snprintf configure check](https://sourceforge.net/p/log4cpp/patches/49/)
* https://src.fedoraproject.org/rpms/log4cpp/c/ac133c1690513a7ee8e8c5986db86e418a981644?branch=rawhide
