* [Fix autoconf check to be C99 compliant](https://sourceforge.net/p/mikmod/bugs/29/)
* https://src.fedoraproject.org/rpms/libmikmod/c/6bba01a215fe006c459e6e975f55517fbc3f4d71?branch=rawhide
