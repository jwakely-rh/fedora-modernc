* [Fixes for improved C99 compatibility](https://github.com/vdudouyt/usbsniff/pull/3)
* https://src.fedoraproject.org/rpms/usbsniff/c/5339a5d1ee8ac271b18c10f4592bc09463b201b4?branch=rawhide
