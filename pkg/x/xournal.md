* [Fix implicit function declarations by including config.h in ttsubset/*.c](https://sourceforge.net/p/xournal/code/ci/430d9502547856943df531b700bb5843fcc8bd1f/)
* [Another implicit function declaration/header inclusion fix](https://sourceforge.net/p/xournal/patches/93/)
* https://src.fedoraproject.org/rpms/xournal/c/dacd5bf30ec95debca62946868d9450f68ea54de?branch=rawhide
