* [Fix return type of main function](https://pagure.io/xmlto/c/8e34f087bf410bcc5fe445933d6ad9bae54f24b5?branch=master)
* [fix -Wimplicit-int for ifsense](https://pagure.io/xmlto/c/1375e2df75530cd198bd16ac3de38e2b0d126276?branch=master)
* https://src.fedoraproject.org/rpms/xmlto/c/571fc033c0ff5d6cf448e2ca20d8ae8ac61a7cb8?branch=rawhide
