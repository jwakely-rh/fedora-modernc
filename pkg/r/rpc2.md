* [Add missing function prototypes for rp2gen code.](https://github.com/cmusatyalab/coda/commit/f9b366855c127c4ac95af04e94e19a057b7e2f51)
* [Cleanup formatting](https://github.com/cmusatyalab/coda/commit/8b432a0268499edecd2e5bab90a17debea8eb33f)
* https://src.fedoraproject.org/rpms/rpc2/c/fcf3867a91e99e972b954fdbbb3d16ab1bbc3fe3?branch=rawhide
