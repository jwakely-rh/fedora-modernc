* [Define printf() before using it in a configure check](https://github.com/dajobe/rasqal/pull/11)
* https://src.fedoraproject.org/rpms/rasqal/c/f2a7ca62916698b2903b68e0b5f81de52dc5df05?branch=rawhide
