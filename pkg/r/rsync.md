* [configure.ac: Avoid implicit int, implicit function declarations](https://github.com/backuppc/rsync-bpc/pull/34)
* https://src.fedoraproject.org/rpms/rsync-bpc/c/b71f9542294c34449d4bf5e104c6916676cd0be1?branch=rawhide
