* [Fix source for clang16](https://github.com/arineng/rwhoisd/pull/2)
* [net-misc/rwhoisd-1.5.9.6-r2 - /.../bison.simple: error: call to undeclared function](https://bugs.gentoo.org/870940)
* https://src.fedoraproject.org/rpms/rwhoisd/c/b27b4a39ffcf4bb0de5c1a0b6e96ee84f44134e3?branch=rawhide
