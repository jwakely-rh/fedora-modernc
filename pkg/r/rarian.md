* [util/rarian-example.c: C99 compatibility fix](https://gitlab.freedesktop.org/rarian/rarian/-/merge_requests/3)
* https://src.fedoraproject.org/rpms/rarian/c/308e10db0d911db664ea8027f71e7e7291b33d61?branch=rawhide
