* [configure.ac: Add missing int return types to main](https://github.com/FirebirdSQL/firebird/pull/7407)
* https://src.fedoraproject.org/rpms/firebird/c/bc46796aca3c109ddd64eb757b0768734fab9e46?branch=rawhide
