* [fcitx-fbterm: Calls to undeclared fcitx_client_enable_ic, fcitx_client_close_ic functions](https://bugzilla.redhat.com/show_bug.cgi?id=2153024)
* [fcitx-gclient: Export fcitx_client_enable_ic, fcitx_client_close_ic](https://github.com/fcitx/fcitx/pull/521)
* https://src.fedoraproject.org/rpms/fcitx/c/426785cd8e990d40be11035015f4cb17a7c90d83?branch=rawhide

Note: The implicit function declarations occurred in the
`fcitx-fbterm` package, but the fix was applied to the `fcitx`
package.

