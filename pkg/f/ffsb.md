* [Add missing declaration of the fhstat function for C99 compatibility](https://sourceforge.net/p/ffsb/bugs/3/)
* https://src.fedoraproject.org/rpms/ffsb/c/393df4c85a1c9543292b4c0773ecaa30a3d1b584?branch=rawhide
