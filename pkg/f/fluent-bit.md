* [CMakeLists: Avoid implicit function declarations on GNU/Linux](https://github.com/fluent/fluent-bit/pull/6704)
* https://src.fedoraproject.org/rpms/fluent-bit/c/c4a5e1f6ae958c21bccef19f54829b4fe556a0db?branch=rawhide
