* [Add missing declaration for `tr_strcasestr`](https://github.com/transmission/transmission/commit/139f3a3f4b8bef5858739a8f08bd2ad9911f55c7)
* https://src.fedoraproject.org/rpms/fragments/c/3f362dee9eb7661e405affebabeda6e1f89db01b?branch=rawhide (does not currently build)
