* [Remove our own copy of SDL_rotozoom in favor of SDL_gfx's.](https://sourceforge.net/p/freedroid/code/ci/76aaf62aef54212e2ef7becb8fee8b97c8e4f9d7)
* https://src.fedoraproject.org/rpms/freedroid/c/31486cfd3f2e2e580e48e19120159338a52fe79c?branch=rawhide
