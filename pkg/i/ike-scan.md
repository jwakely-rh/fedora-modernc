* [Fix a couple autoconf tests on Xcode 12](https://github.com/royhills/ike-scan/commit/c9ef0569443b03fda5339911acb8056a73c952de)
* [acinclude.m4: fix -Wimplicit-int errors (Clang 16+ compat)](https://github.com/royhills/ike-scan/commit/9949ce4bdf9f4bcb616b2a5d273708a7ea9ee93d)
* https://src.fedoraproject.org/rpms/ike-scan/c/f0e7cc453ddeb6450821b51b9d688c9567baeff2?branch=rawhide
