* [[Iftop-users] Improve C99 compatibility of the configure script](https://lists.beasts.org/pipermail/iftop-users/2022-December/000513.html)
* https://src.fedoraproject.org/rpms/iftop/c/8cca6e6a1ec81e228c7363378f65172cda20f2d4?branch=rawhide
