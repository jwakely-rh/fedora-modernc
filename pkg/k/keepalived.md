* [config parser: Fix segfault caused by extra '}' and other parser fixes](https://github.com/acassen/keepalived/commit/516032ec39169d05c613de0e8ee10845658748ff) (includes `configure.ac` change, the check always failed because a syntax error, not related to C99 porting)
* https://src.fedoraproject.org/rpms/keepalived/c/c78adb070ca5926239e1e959da934e8c5627e3df?branch=rawhide
