* [Added missing pm_linux/finddefault.h and added Doxygen output to doc.](https://github.com/PortMidi/portmidi/commit/9abb601e41ff2872feffa51131c40c5aed2bbaa0)
* [Implemented CreateVirtualInput and CreateVirtualOutput on linux.](https://github.com/PortMidi/portmidi/commit/cf3fb35415987df6fc6d6950c7b8272a2d454cd8)
* https://src.fedoraproject.org/rpms/portmidi/c/cde2ec4b36596837cbcb9e6d6d4c5b4b85a5d93c?branch=rawhide
