* [Unified CMake-based build system](https://github.com/mozilla/mozjpeg/commit/6abd39160c5a3762e9ebe024e75407665093e715) (the new CMake check for `RIGHT_SHIFT_IS_UNSIGNED` includes `<stdlib.h>` for the `exit` function)
* https://src.fedoraproject.org/rpms/pgadmin4/c/271839f52113d0a6a95c6c2e2318b5e7060ecca4?branch=rawhide
