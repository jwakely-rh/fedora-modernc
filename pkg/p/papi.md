* [configure: Avoid implicit ints and implicit function declarations](https://bitbucket.org/icl/papi/pull-requests/406)
* https://bugzilla.redhat.com/show_bug.cgi?id=2148723
* https://src.fedoraproject.org/rpms/papi/c/1c4d45e252ed86e11792b4aeb9903e96b7299be2?branch=rawhide
