* [Digest-Nilsimsa: Add more prototypes to nilsimsa.h for C99 compatibility](https://rt.cpan.org/Public/Bug/Display.html?id=145874)
* https://src.fedoraproject.org/rpms/perl-Digest-Nilsimsa/c/4d615795d06d3cadc9e1dbd33c6d74c122cb9edf?branch=rawhide
