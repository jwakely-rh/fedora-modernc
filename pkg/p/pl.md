* [cmake: Fix C99 compatibility issues in GCC built-in detection](https://github.com/SWI-Prolog/swipl-devel/pull/1089)
* https://src.fedoraproject.org/rpms/pl/c/1c6009e6a44e6ed18626a0c6803663fa3d3fac50?branch=rawhide
