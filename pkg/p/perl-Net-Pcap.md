* [Avoid implicit ints during feature probing](https://rt.cpan.org/Public/Bug/Display.html?id=145963)
* https://src.fedoraproject.org/rpms/perl-Net-Pcap/c/6623043bf563c79bad10058ec44140fce039222a?branch=rawhide
