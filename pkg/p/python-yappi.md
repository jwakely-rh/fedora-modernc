* [distutils.ccompiler: Make has_function work with more C99 compilers](https://github.com/pypa/distutils/pull/195)
* https://bugzilla.redhat.com/show_bug.cgi?id=2153038
* https://src.fedoraproject.org/rpms/python-yappi/c/7c6a8fca4a9cb33459a657fd940c688931b808f9?branch=rawhide
