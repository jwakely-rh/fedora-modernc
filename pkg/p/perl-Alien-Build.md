* [Avoid C99-related errors in expected-compiler-error test](https://github.com/PerlAlien/Alien-Build/pull/383)
* https://src.fedoraproject.org/rpms/perl-Alien-Build/c/9f2b8856c51910f627a0ee2ce9054a9897d34044?branch=rawhide
