* [mxTextTools: Replace Py_UNICODE_COPY with memcpy](https://github.com/mcfletch/simpleparse/pull/21)
* https://src.fedoraproject.org/rpms/python-simpleparse/c/420c895f574d408fffcc0770c68e9e176107fe6f?branch=rawhide
