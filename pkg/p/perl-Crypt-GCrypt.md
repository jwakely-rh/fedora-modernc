* [C99 compatibility fix for Makefile.PL](https://rt.cpan.org/Public/Bug/Display.html?id=146068)
* https://src.fedoraproject.org/rpms/perl-Crypt-GCrypt/c/24a5a746861c2177232926009b6e536aaf85c51d?branch=rawhide
