* [Fix meson/configure _Thread_local checks for C99 compatibility](https://github.com/p11-glue/p11-kit/pull/451)
* https://src.fedoraproject.org/rpms/p11-kit/c/b90c845b4f17af5a81f38c435e3eb512cd07125b?branch=rawhide
