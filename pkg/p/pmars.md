* [pmars: Build in C89 mode because of internal inconsistency about sighandler definition](https://bugzilla.redhat.com/show_bug.cgi?id=2155640)
* https://src.fedoraproject.org/rpms/pmars/c/3145a08fc5403dc72fc6f69401770c7632d6c1f3?branch=rawhide
