* [configure.ac: Fixes for improved C99 compatibility](https://github.com/performancecopilot/pcp/pull/1727)
* https://src.fedoraproject.org/rpms/pcp/c/709b74898f5f3e63eb6a336ee974a4ba8f3ff73b?branch=rawhide
