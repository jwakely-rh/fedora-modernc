* [Improve compatibility with strict(er) C99 compilers](https://github.com/niner/inline-python-pm/pull/39)
* https://src.fedoraproject.org/rpms/perl-Inline-Python/c/ab68e905166ed1c900c09042c5586e3c110b4eae?branch=rawhide
