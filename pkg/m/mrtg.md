* [configure.ac: Avoid implicit declaration of the exit function](https://github.com/oetiker/mrtg/pull/104)
* https://src.fedoraproject.org/rpms/mrtg/c/c1080602cf39175e6030c35ec809e7eacd53ad47?branch=rawhide
