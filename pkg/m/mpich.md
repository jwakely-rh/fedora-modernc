* [aclocal_cc.m4: Remove use of implicit int from weak symbol check](https://github.com/pmodels/mpich/pull/6301)
* [configure.ac: Fix max_align_t alignment check](https://github.com/pmodels/mpich/pull/6302)
* [configure.ac: Do not call undeclared exit function in __thread check](https://github.com/pmodels/json-c/pull/2)
* https://src.fedoraproject.org/rpms/mpich/c/3aac8d8e5b8fbcec5242b535a85aaa6e82ee0988?branch=rawhide
