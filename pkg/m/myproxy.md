* [configure.ac: Avoid calling the undeclared exit function](https://github.com/globus/globus-toolkit/pull/132)
* https://src.fedoraproject.org/rpms/myproxy/c/50e3fec8440ffbb022155cb773adc2c457c1470e?branch=rawhide
