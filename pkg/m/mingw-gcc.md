* [libsanitizer: Avoid implicit function declaration in configure test](https://gcc.gnu.org/git/?p=gcc.git;a=commitdiff;h=7d7146102365f708a37401c902fce2f4024b546a)
* [libiberty: Fix C89-isms in configure tests](https://gcc.gnu.org/git/?p=gcc.git;a=commitdiff;h=63f3eae53683e857818c4bd3d1de719e1310e22a)
* https://src.fedoraproject.org/rpms/mingw-gcc/c/b4a60afda25fe02fe6e5edd82433135f4f9cd56c?branch=rawhide
