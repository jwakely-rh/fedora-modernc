* [Avoid implicit function declarations in pthread_setname_np probe](https://github.com/MoarVM/MoarVM/pull/1730)
* https://src.fedoraproject.org/rpms/moarvm/c/58de3fa0108d46c0f790cc5ac23f187a9a968164?branch=rawhide
