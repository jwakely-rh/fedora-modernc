* [netstat-nat: Missing forward declarations impact C99 compatibility](https://bugzilla.redhat.com/show_bug.cgi?id=2151453)
* https://src.fedoraproject.org/rpms/netstat-nat/c/1b912adf92cc24e3fcb4f2e2b65139c87ffcfe1c?branch=rawhide
