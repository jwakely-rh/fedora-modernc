* [configure.ac: Add missing comma to atomic_thread_fence check](https://github.com/ntp-project/ntp/pull/13)
* https://src.fedoraproject.org/rpms/ntp-refclock/c/a5e9d642946450c730ff8bc644824525cf92c0fd?branch=rawhide
