* [configure: Fix C99 compatibility issue](https://github.com/fadden/nulib2/pull/15)
* https://src.fedoraproject.org/rpms/nulib2/c/29669772e767f8c7abe0fdb1429941fe48d49e81?branch=rawhide
