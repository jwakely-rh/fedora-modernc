* https://bugzilla.redhat.com/show_bug.cgi?id=2143992
* https://src.fedoraproject.org/rpms/gdb/c/910689b88d5581aa6f0934278d21839ccf30da18?branch=rawhide

Actual fix comes from libiberty in GCC:

* [libiberty: Fix C89-isms in configure tests](https://gcc.gnu.org/git/?p=gcc.git;a=commitdiff;h=885b6660c17fb91980b5682514ef54668e544b02)
