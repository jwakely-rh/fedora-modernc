* [-Wimplicit-function-declaration in pthread_setname_np check (missing _GNU_SOURCE)](https://gitlab.freedesktop.org/gstreamer/gstreamer/-/issues/1542)
* [meson: fix check for pthread_setname_np()](https://gitlab.freedesktop.org/gstreamer/gstreamer/-/commit/a2c7398a1c3a385a5f0a30c29b3cede69b2180bc)
* https://src.fedoraproject.org/rpms/gstreamer1/c/8891ce9c8ad318252a8b31086041959e5a6815dd?branch=rawhide
