* [Update Gnulib to v0.1-1157-gb03f418.](https://git.savannah.gnu.org/cgit/guile.git/commit/?id=7e641595cd9b45ce7339e21c20a8ab81af9278f6)
* [Update Gnulib to v0.1-4379-g2ef5a9b4b](https://git.savannah.gnu.org/cgit/guile.git/commit/?id=a91b95cca2d397c84f8b9bbd602d40209a7092ce)
* [configure.ac fix for C99 compatibility](https://debbugs.gnu.org/cgi/bugreport.cgi?bug=60022), [mailing list thread](https://lists.gnu.org/archive/html/bug-guile/2022-12/msg00017.html)
* https://src.fedoraproject.org/rpms/guile/c/7fcab121be7d2bc8e3e48d0da476127b86cec90c?branch=rawhide
