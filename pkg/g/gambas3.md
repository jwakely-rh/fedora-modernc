* [C99 compatibility fixes for configure scripts](https://gitlab.com/gambas/gambas/-/merge_requests/286)
* https://src.fedoraproject.org/rpms/gambas3/c/722eb978a682aa3adfb481d5c883c76d02124a86?branch=rawhide
