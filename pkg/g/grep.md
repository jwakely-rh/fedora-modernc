* [More fixes for compilers that reject K&R function definitions.](https://git.savannah.gnu.org/cgit/autoconf.git/commit/?id=bf5a75953b6d504f0405b1ca33b039b8dd39eef4) (actually an autoconf fix; grep upstream used a pre-release to create the configure script)
* https://src.fedoraproject.org/rpms/grep/c/e9c80f0c519b373727bc76669a9e6a00da904210?branch=rawhide
