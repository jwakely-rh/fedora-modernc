* Upstream moved to the Meson build system, making autoconf fixes
  non-applicable there.
* [Use single header with GtkSource](https://gitlab.gnome.org/GNOME/libgda/-/commit/3415fc0a96bb71ac0f611f0737e839b5322a9174) (for `gtk_source_language_get_name` declaration)
* https://src.fedoraproject.org/rpms/libgda5/c/546553a0bb7dcb5bdc4ee1b839bc12d1788eed0f?branch=rawhide
