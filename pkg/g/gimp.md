* [configure.ac: Improve C99 compatibility of IPC_RMID check](https://gitlab.gnome.org/GNOME/gimp/-/merge_requests/810)
* https://src.fedoraproject.org/rpms/gimp/c/d92f136c03eb3af39deb12d760639fad24ce1953?branch=rawhide
