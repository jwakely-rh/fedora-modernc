* [configure.ac fix for C99 compatibility](https://debbugs.gnu.org/cgi/bugreport.cgi?bug=60022), [mailing list thread](https://lists.gnu.org/archive/html/bug-guile/2022-12/msg00017.html)
* https://src.fedoraproject.org/rpms/guile30/c/47f608ff4988547350e722606890698e3ec59e95?branch=rawhide
