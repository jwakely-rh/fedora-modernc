* [m4/fp_leading_underscore.m4: Avoid implicit exit function declaration](https://gitlab.haskell.org/ghc/ghc/-/merge_requests/9394)
* https://src.fedoraproject.org/rpms/ghc9.4/c/c4e0f48c4562a293267c805f42525680cfa09874?branch=rawhide
